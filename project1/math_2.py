
'''This file contains code for some more basic mathematical operations '''

def square(entity): 
    '''
    This is a simple function to get square of a number
    '''
    return entity*entity

def cube(entity): 
    '''
    This is a simple function to get cube of a number
    '''
    return entity*entity*entity

def absolute(entity): 
    '''
    This is a simple function to get absolute value of a number
    '''
    if entity>0:
        return entity
    else:
        return -entity
        