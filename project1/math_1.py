''''This file contains code for basic mathematical ooperations '''

def add(first_entity, second_entity):
    '''
    This is a simple function to add two entities
    '''
    return first_entity + second_entity


def subtract(first_entity, second_entity):
    '''
    This is a simple function to subtract two entities
    '''
    return first_entity - second_entity


def multiply(first_entity, second_entity):
    '''
    This is a simple function to multiply two entities
    '''
    return first_entity * second_entity


def divide(numerator, denominator):
    return float(numerator) / denominator
