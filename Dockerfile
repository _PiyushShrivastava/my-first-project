FROM ubuntu:16.04

MAINTAINER Your Name "youremail@domain.tld"

RUN apt-get update -y && \
    apt-get install -y python-pip python-dev && \
     apt-get install -y python-flask 


WORKDIR /app


COPY . /app

ENTRYPOINT [ "python" ]

CMD [ "app.py" ]